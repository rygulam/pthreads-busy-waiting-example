
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> 
#include <iostream>
#include <iomanip>
#include <streambuf>
#include <unistd.h>

#include "CStopWatch.h"

double estimate = 0;
int flag = 0;

struct dataToPass{
    int id;
    int numSamples;
};

/*-------------------------------------------------------------------*/
void* getSamples(void* data) {
   
    dataToPass* d = (dataToPass*)data;
    long numSamples = d->numSamples;
    int myRank = d->id;

    double x, y;
    // double* estimate = new double();
    int numInCircle = 0.00;

    for(int i=0; i< numSamples; i++){
        x = ((double) std::rand() / (double) RAND_MAX) * 2.0 - 1.0;
        y = ((double) std::rand() / (double) RAND_MAX) * 2.0 - 1.0;

        if(x*x + y*y <=1){
            numInCircle++;
        }
    }
    // estimate = (4.0*numInCircle/((double) numSamples));

    
    while(flag != myRank)
    {
        // std::cout << "Waiting: " << myRank <<"\n";   
    };
    // std::cout << "MyRank:" << myRank << "\n";
    // std::cout << (4.0*numInCircle)/numSamples << "\n";
    estimate = estimate + (4.0*numInCircle/numSamples);
    // std::cout << estimate << "\n";
    flag++;
    // std::cout << "Flag is now:" << flag << "\n";

    delete d;
    return NULL;
}  



int main() {
   long        threadMin, threadMax, threadStep;
   long        sampleMin, sampleMax, sampleStep;
   long        numTrials;
   pthread_t*  thread_handles; 
   CStopWatch  timer;
//    void*       retValue;
//    double      estimate;
   dataToPass* d;

   threadMin  = 1; 
   threadMax  = 100;
   threadStep = 10;

   sampleMin  = 100000;
   sampleMax  = 100001;
   sampleStep = 100;

   numTrials  = 10;
   
   thread_handles = new pthread_t[threadMax]; 

   for(int numThreads=threadMin; numThreads<threadMax; numThreads+=threadStep){

      for(long numSamples=sampleMin; numSamples < sampleMax; numSamples+=sampleStep){
         for(int curTrial=0; curTrial<numTrials; curTrial++){
            estimate = 0.0;
            flag = 0;
            timer.startTimer();
            for (long thread = 0; thread < numThreads; thread++){
                d = new dataToPass();
                d->id = thread;
                d->numSamples = numSamples/numThreads;
                // std::cout << "Main MyRanks:" << thread <<"\n";
                // pthread_create(&thread_handles[thread], NULL, getSamples, (void*) (numSamples/numThreads)); 
                 pthread_create(&thread_handles[thread], NULL, getSamples, (void*) d);  
            }

            for (int thread = 0; thread < numThreads; thread++){
                pthread_join(thread_handles[thread], NULL);            
                // estimate += *(double*)retValue;
                // delete (double*)retValue;
            }
            estimate /= (double)numThreads;
            timer.stopTimer();

            std::cout << std::fixed << std::setprecision(4) << estimate << " " 
                      << std::setprecision(0) << numThreads << " " << numSamples << " " 
                      << std::setprecision(4)<< timer.getElapsedTime() << "\n";
         }
      }
   }

   delete thread_handles;

   return 0;
} 