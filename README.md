### Calculating Pi with pThreads ###

A basic example showing how to calculate pi and return values from a pThreads function. Note that dynamically allocated and returned memory must be deallocated as well!

### Instructions ###

1. Modify the code from the previous reflection (that calculated the value of p) to meet the following criteria:
- The variable used to calculate the estimate of p from each thread (estimate in the code provided)
must now be single, shared (i.e. global) variable
- The busy-waiting paradigm (i.e. the use of flag) must be implemented to ensure that more than one
thread does not write to estimate at any given time
- The function called by each thread must recieve two parameters � the ID or rank of the current thread
and the number of samples to draw.

2. What happens to your results when using different levels of optimization (i.e. using -O0 and -O2 with g++)?

To compile and run from command line:
```
g++ main.cpp CStopWatch.cpp
./a.out
```
or
```
   cd Default && make all
```

To compile and run with Docker:
```
docker run --rm -v c:/Users/Michael/Desktop/pthreads-busy-waiting-example:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp make all

docker run --rm -v c:/Users/Michael/Desktop/pthreads-busy-waiting-example:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp ./pThreads_Timing
```

### REFLECTION 4 ###

a)  What was the major question or topic addressed in the classroom?

	The main topic for this past week is optimization of code concerning pThreads and implementation of the busy waiting technique into our previous program that estimates the value of pi.

b)  Summarize what you learned during this week of class. Note any particular features, concepts, code,or terms that you believe are important.�  References and examples of your completed work must be included! If you include a screenshot,code, formula, etc., you must also explain it clearly.�  Note that naming a concept does not demonstrate learning.   If you name a concept or topic,spend at least 1-2 sentences explaining what it is or how it works.

	This week of class we discussed how to handle nested loops in parallelized code. In general, performing operations inside of the outer loops is cleaner because of many reasons, among them being that they retain better granularity and they are easier to split up. In order to share variables between threads, we have decided to take them outt of the main() function and make them global variables for this program. We discussed some ways to solve problems caused by critical sections of code, which are usually non-parallelizable. This methods involve the use of mutexes and semaphores. Mutex is short for "mutual exclusion object." and it is an object that handles multiple threads taking turns accessing a shared resource. A semaphore is a flag variable or abstract data type that is used to control access to a shared resource. The key difference between the two methods is that mutexes are controlled entirely by the thread in use, while a semaphore can be controlled by an outside thread. The run results of this week's programming assignment are included in screenshots inside of the "results" folder of the repository. They are screenshots taken of the program being run with optimization on and off for comparison purposes. busy waiting was used in this program to avoid race conditions, when multiple threads try to write to  the same location at the same time. This will often result in computational errors or even a full lockup of your computer. Busy waiting techniques involve manually setting control variables in our code that force our threads to take turns reading and writing data to our global variables. This prevents them from getting into a traffic jam as mentioned earlier. We used a struct that held the current thread id and the number of samples per thread to help in this process.

c)  What was the most difficult aspect of what you learned in class & the lab experience?  Why was this the most difficult aspect?  What can you do to make it easier for you?  Particularly focus on what you accomplished in class and what kept you from completing the assignment in class.
	
	The most difficult part of this week's curriculum is the ever increasing usage of pointers in our code. I realize they are helpful and even neccessary because of the nature of threads and how their access to memory locations work but I find them difficult to wrap my head around sometimes and they add to the difficulty of an already confusing, but fascinating topic. As far as this assignment goes, the hardest part was ensuring that the threads communicate with eachother well with the use of my flag.

d)  Compare and contrast your current code to the previous code.  How does it differ?  What happens when you turn optimization on and off?

	Compared to the previous implementation of this pi-estimation-program, this version tends to take longer to compute but also tends to achieve more consistently accurate, or at least consistent results accross both sample and thread counts. When I turn optimization on, there is a minor but noticable increase in speed of processing. I imagine with more complex programs where the code can get much messier and involved, optimiazation can result in further improvements, as long as the optimization method does not break the program. Said optimization errors can be prevented by marking variables in critical sections of code as "volatile", this way the optimization method will ignore their order and will not move them around.